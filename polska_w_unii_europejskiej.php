<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle "> Polska w Unii Europejskiej
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
            	Polska w Unii Europejskiej
Perspektywa europejska – miejsce Polski w Europie, wpływ Unii Europejskiej na nasz kraj, a także wpływ Polski na politykę wspólnoty – odgrywa kluczową rolę w działaniach Fundacji. W 1994 roku rozpoczęto program "Polska w Unii Europejskiej". W ramach programu podejmowano różnorodne działania zmierzające do integracji Polski z UE. W pierwszym etapie zadaniem programu była ocena przebiegu negocjacji akcesyjnych oraz identyfikacja problemów pojawiających się na drodze Polski do struktur europejskich. Niezwykle ważnym aspektem działań było również informowanie społeczeństwa o korzyściach płynących z integracji oraz przekonywanie do poparcia przystąpienia do Unii Europejskiej. Po akcesji Polski do wspólnoty Instytut Studiów Strategicznych zajmuje się kluczowymi elementami polityki unijnej w wymiarze politycznym, społecznym i gospodarczym.

				<ul><b>Ważniejsze wydarzenia programu "Polska w Unii Europejskiej":</b> 

	<li>konferencja międzynarodowa "Polska w Europie" – czerwiec 1994</li>
<li>konferencja międzynarodowa "Stan przygotowań Polski, Czech, Węgier, Estonii i Słowenii do negocjacji z UE" – kwiecień 1998</li>
<li>publikacja 102 powody dla których Polska powinna wejść do Unii Europejskiej – marzec 2003</li>
<li>konferencja międzynarodowa "Rozszerzenie Unii Europejskiej i jego wpływ na współpracę transgraniczną i międzyregionalną" – październik 2003 </li>

				</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
