<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Program INDEX Pamięci Polaków Zamordowanych i Represjonowanych przez Hitlerowców za Pomoc Żydom
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
         Nie wiemy dokładnie ilu Polaków wzięło udział w ratowaniu swoich żydowskich współobywateli od śmierci z rąk niemieckiego okupanta w latach II wojny światowej. Liczby osób zidentyfikowanych z imienia i nazwiska wciąż stanowią tylko wierzchołek góry lodowej. Temat Ratujących w coraz szerszym stopniu stanowi integralną część polskiej narracji o II wojnie światowej. Program INDEX został powołany, by poszerzać i upowszechniać wiedzę na ten temat.<br>

Program INDEX służy ustaleniu nazwisk i biografii Polaków oraz obywateli polskich innych narodowości (np. ukraińskiej, białoruskiej, litewskiej, ormiańskiej, niemieckiej), którzy na skutek działań nazistów ponieśli śmierć lub spotkali się z inną formą represji za pomoc udzielaną Żydom podczas II wojny światowej. Program jest podzielony na trzy obszary tematyczne: badania naukowe, upamiętnienie, edukacja i informacja. W pierwszym etapie programu została przeprowadzona wnikliwa, rzetelna kwerenda w archiwach niemieckich, austriackich, ukraińskich, białoruskich, litewskich, izraelskich, brytyjskich, amerykańskich i polskich. Prace te były prowadzone pod kierunkiem uznanych autorytetów naukowych. Honorowym przewodniczącym Rady Programowej został prof. Władysław Bartoszewski. Informacje i dokumenty były również zbierane za pośrednictwem mediów, u bezpośrednich 
świadków i ich spadkobierców. Efekty badań były i są prezentowane podczas seminariów naukowych i konferencji.<br>

Kolejnym etapem programu jest stworzenie strony internetowej „Małopolska Mapa Pomocy Żydom” dokumentującej i wyświetlającej informacje dotyczące mieszkańców Małopolski, którzy w czasie II wojny światowej pomagali osobom żydowskiego pochodzenia i spotkały ich za to represje ze strony niemieckiego okupanta. Mapa w nowoczesny i atrakcyjny dla odbiorcy sposób zaprezentuje miejsca tragicznych wydarzeń, ich wojenną historię a także przedstawi wszystkie dostępne informacje dotyczące pomagających i represjonowanych pochodzące z kwerendy historycznej, archiwistycznej, zebranej podczas wywiadów w terenie i w ramach dokumentacji etnograficznej. Strona internetowa będzie również zawierać informacje o osobach pochodzenia żydowskiego z terenu obecnego województwa małopolskiego którzy przeżyli wojnę.<br>

Chcemy złożyć szczególne podziękowania partnerom Fundacji Instytut Studiów Strategicznych, zaangażowanym w realizację programu Index: Fundacji PZU, Narodowemu Centrum Kultury, Fundacji Kronenberga, Fundacji „Pamięć i Przyszłość” oraz Województwu Małopolskiemu.
					</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
