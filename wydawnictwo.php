<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
									 
									<div class="item active">
										<div class="row wydawnictwo-bg">
											
											<h3>Księga Sprawiedliwych wśród Narodów Świata. Tom 1-2. KOMPLET </h3>
											<div class="okl img-responsive"><img src="img/publikacje/1.jpg" alt="okładka"/></div>
										
												<p>autor: <span>Israel Gutman </span></p>
												<p>
													<span>"Kto ratuje jedno życie, ratuje cały świat".</span><br><br>
				Wśród prawie 22 tysięcy Sprawiedliwych - ludzi, którzy za ratowanie Żydów podczas II wojny światowej zostali uhonorowani przez Instytut Yad Vashem w Jerozolimie tytułem Sprawiedliwego wśród Narodów Świata - ponad 6 tysięcy to Polacy. W niniejszym wydaniu Księgi znajdziecie Państwo biogramy tych, którzy zostali uhonorowani do 2000 roku. Historie opisane w Księdze to dzieje zwykłych ludzi, obcych i sąsiadów a jednocześnie - bohaterów, którzy nie bacząc na grożącą im i najbliższym karę śmierci, nieśli pomoc Żydom. Pomagali, bo tak nakazywało im sumienie, ich chrześcijańska wiara, zwyczajne poczucie przyzwoitości. Wierzymy, że publikacja umocni wiarę w człowieka i stanie się dla kolejnych pokoleń drogowskazem postępowania w trudnych sytuacjach. My, Polacy nie możemy zapominać o tych, dla których najważniejszy był drugi człowiek.
											</p>
											<p class="price"><b>Cena detaliczna: 135 zł</b></p>

										</div>

												 <a class="left carousel-control" href="#wydawnictwo-slide" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left"></span>
												 </a>
												 <a class="right carousel-control" href="#wydawnictwo-slide" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right"></span>
												 </a>



									   </div>
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
								 <!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
									 
									<div class="item">
										<div class="row wydawnictwo-bg">
											
											<h3>Sprawiedliwi i ich świat. Markowa w fotografii Józefa Ulmy</h3>
											<div class="okl img-responsive"><img src="img/publikacje/22.jpg" alt="okładka"/></div>
										
												<p>autor: <span>Mateusz Szpytma</span></p>
												<p>
												Józef Ulma (1900-1944) był ogrodnikiem i działaczem społecznym. Jako fotograf amator dokumentował życie rodzinnej Markowej, jednej z podkarpackich wsi. Zachowało się wiele zdjęć, na których uwiecznił żonę Wiktorię oraz swoje małe dzieci: Stasię, Basię, Władzia, Franusia, Antosia i Marysię. W 1942 roku Ulmowie przyjęli pod swój dach poszukujących pomocy Żydów z Markowej i Łańcuta, ośmioosobową rodzinę Grünfeldów, Didnerów i Goldmanów. Półtora roku później, 24 marca 1944 roku, wszyscy zostali zamordowani przez niemieckich żandarmów. <br><br>
Józef i Wiktoria Ulmowie w 1995 roku zostali uhonorowani pośmiertnie tytułem Sprawiedliwi wśród Narodów Świata. W Watykanie toczy się proces beatyfikacyjny obejmujący całą rodzinę. Ich imieniem nazwano jedyne w Polsce Muzeum Polaków Ratujących Żydów, znajdujące się w Markowej.

											</p>
											<p class="price"><b>Cena detaliczna: 39 zł</b></p>

										</div>

												 <a class="left carousel-control" href="#wydawnictwo-slide" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left"></span>
												 </a>
												 <a class="right carousel-control" href="#wydawnictwo-slide" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right"></span>
												 </a>



									   </div>
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
								 <!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
									 
									<div class="item">
										<div class="row wydawnictwo-bg">
											
											<h3>Współczesne stosunki polsko-ukraińskie </h3>
											<div class="okl img-responsive"><img src="img/publikacje/4.jpg" alt="okładka"/></div>
										
												<p>autor: <span>Piotr Kuspys </span></p>
												<p>
				Autor kreśli pozytywny bilans stosunków dwóch krajów w latach 1991-2008, choć nie unika on także spraw trudnych lub kontrowersyjnych. 2 grudnia 1991 roku Polska jako pierwsze państwo na świecie oficjalnie uznała suwerenność Ukrainy. 18 maja 1992 roku oba państwa podpisały traktat o dobrym sąsiedztwie, przyjaznych stosunkach i współpracy wyznaczający formalno-prawne ramy relacji polsko-ukraińskich. Współpracy polsko-ukraińskiej nie podważyło ochłodzenie stosunków w pierwszej połowie lat dziewięćdziesiątych. 
											</p>
											<p class="price"><b>Cena detaliczna: 35 zł</b></p>

										</div>

												 <a class="left carousel-control" href="#wydawnictwo-slide" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left"></span>
												 </a>
												 <a class="right carousel-control" href="#wydawnictwo-slide" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right"></span>
												 </a>



									   </div>
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
								 <!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
									 
									<div class="item ">
										<div class="row wydawnictwo-bg">
											
											<h3>Współczesne stosunki polsko-ukraińskie </h3>
											<div class="okl img-responsive"><img src="img/publikacje/3.jpg" alt="okładka"/></div>
										
												<p>autor: <span>Piotr Kuspys </span></p>
												<p>
				Autor kreśli pozytywny bilans stosunków dwóch krajów w latach 1991-2008, choć nie unika on także spraw trudnych lub kontrowersyjnych. 2 grudnia 1991 roku Polska jako pierwsze państwo na świecie oficjalnie uznała suwerenność Ukrainy. 18 maja 1992 roku oba państwa podpisały traktat o dobrym sąsiedztwie, przyjaznych stosunkach i współpracy wyznaczający formalno-prawne ramy relacji polsko-ukraińskich. Współpracy polsko-ukraińskiej nie podważyło ochłodzenie stosunków w pierwszej połowie lat dziewięćdziesiątych. 
											</p>
											<p class="price"><b>Cena detaliczna: 35 zł</b></p>

										</div>

												 <a class="left carousel-control" href="#wydawnictwo-slide" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left"></span>
												 </a>
												 <a class="right carousel-control" href="#wydawnictwo-slide" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right"></span>
												 </a>



									   </div>
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->
									 
									<div class="item">
										<div class="row wydawnictwo-bg">
											
											<h3>Cena Poświęcenia. Zbrodnie na Polakach za pomoc udzielaną <br>Żydom w rejonie Ciepielowa. </h3>
											<div class="okl img-responsive"><img src="img/publikacje/cena_poswiecenia.png" alt="okładka"/></div>
										
												<p>autorzy: <span>Jacek Młynarczyk i Sebastian Piątkowski</span></p>
												<p>
				Książka Jacka Młynarczyka i Sebastiana Piątkowskiego należy do bardzo nielicznych dotychczas publikacji traktujących kwestię pomocy udzielanej przez Polaków ludności żydowskiej w sposób całkowicie naukowy, z pełnym wykorzystaniem warsztatu historyka. Stanowi ponadto całkowicie pionierskie dzieło jako monografia szczegółowo omawiająca zbrodnie na Polakach za udzielanie pomocy Żydom w wybranym rejonie okupowanych ziem polskich.<br>
										
											</p>
											<p class="price">
												<b>Cena detaliczna: 34 zł</b>
											</p>
											

										</div>

												 <a class="left carousel-control" href="#wydawnictwo-slide" data-slide="prev">
													<span class="glyphicon glyphicon-chevron-left"></span>
												 </a>
												 <a class="right carousel-control" href="#wydawnictwo-slide" data-slide="next">
													<span class="glyphicon glyphicon-chevron-right"></span>
												 </a>



									   </div>
<!-- /////////////////////////////////////POJEDYNCZE WYDAWNICTWO////////  -->