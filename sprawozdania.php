<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Sprawozdania ISS</h1>
            	<h2>Zestaw sprawozdańz działalności Fundacji Instytut Studiów Strategicznych</h2>
            
            </div>
        </div>
    </section>
     <section class="container"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Sprawozdania z działaności Instytutu Studiów strategicznych za lata poprzednie                 	<!--	<span>2016-07-31</span> -->
              	 </header>
            	
 <h2>Sprawozdania finansowe</h2>
    <ul class=" my-content">
		<li> <a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2003.pdf">Sprawozdanie finansowe za rok 2003</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2004.pdf">Sprawozdanie finansowe za rok 2004</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2005.pdf">Sprawozdanie finansowe za rok 2005</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2006.pdf">Sprawozdanie finansowe za rok 2006</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2007.pdf">Sprawozdanie finansowe za rok 2007</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2008.pdf">Sprawozdanie finansowe za rok 2008</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2009.pdf">Sprawozdanie finansowe za rok 2009</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2010.pdf">Sprawozdanie finansowe za rok 2010</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2011.pdf">Sprawozdanie finansowe za rok 2011</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2012.pdf">Sprawozdanie finansowe za rok 2012</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2013.pdf">Sprawozdanie finansowe za rok 2013</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2014.pdf">Sprawozdanie finansowe za rok 2014</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2015.pdf">Sprawozdanie finansowe za rok 2015</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2016.pdf">Sprawozdanie finansowe za rok 2016</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_finansowe_za_rok_2017.pdf">Sprawozdanie finansowe za rok 2017</a></li>
	</ul>
   <h2>Sprawozdania merytoryczne</h2>
    <ul class=" my-content">
		<li> <a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2003.pdf">Sprawozdanie merytoryczne za rok 2003</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2004.pdf">Sprawozdanie merytoryczne za rok 2004</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2005.pdf">Sprawozdanie merytoryczne za rok 2005</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2006.pdf">Sprawozdanie merytoryczne za rok 2006</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2007.pdf">Sprawozdanie merytoryczne za rok 2007</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2008.pdf">Sprawozdanie merytoryczne za rok 2008</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2009.pdf">Sprawozdanie merytoryczne za rok 2009</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2010.pdf">Sprawozdanie merytoryczne za rok 2010</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2011.pdf">Sprawozdanie merytoryczne za rok 2011</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2012.pdf">Sprawozdanie merytoryczne za rok 2012</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2013.pdf">Sprawozdanie merytoryczne za rok 2013</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2014.pdf">Sprawozdanie merytoryczne za rok 2014</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2015.pdf">Sprawozdanie merytoryczne za rok 2015</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2016.pdf">Sprawozdanie merytoryczne za rok 2016</a></li>
		<li><a href="documents/sprawozdania/sprawozdanie_merytoryczne_za_rok_2017.pdf">Sprawozdanie merytoryczne za rok 2017</a></li>
	</ul>

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
