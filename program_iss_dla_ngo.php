<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Program ISS dla NGO
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
         Program realizowany jest od roku 2008. Celem programu jest wsparcie organizacji pozarządowych w efektywnym działaniu, sprawnym wykorzystywaniu środków krajowych i europejskich, budowaniu regionalnych powiązań. Równie ważnym obszarem działania jest realizowanie projektów o charakterze edukacyjnym i społecznym, wspierającym i aktywizującym nauczycieli, młodzież, liderów społecznych oraz lokalne społeczności. 
 <br> 
<ul>
	<b>Główne cele programu to: </b>
	<li>realizowanie projektów edukacyjno - szkoleniowych wspierających rozwój osobisty i społeczny w mniejszych ośrodkach poza centrami wielkich miast;</li>
	<li>kompleksowe wsparcie informacyjne, szkoleniowe, doradcze i animacyjne dla małopolskich organizacji pozarządowych, liderów społecznych, nauczycieli i młodzieży szkół ponadgimnazjalnych; </li>
	<li>poszerzenie współpracy samorządów z organizacjami pozarządowymi, promocję i budowanie partnerskich relacji międzysektorowych (działania edukacyjne adresowane do pracowników JST i NGOs, promocję dobrych praktyk);</li> 
<li>wzmocnienie integracji sektora pozarządowego w Małopolsce poprzez zbliżanie środowiska pozarządowego, poszerzanie wiedzy i umiejętności w zakresie budowania współpracy oraz inicjowania i wspomagania partnerstwa wewnątrzsektorowego;
	<li>rozwijanie aktywności lokalnej poprzez identyfikowanie i wspieranie liderów lokalnych oraz aktywizowanie instytucji zaangażowanych w rozwój społeczności lokalnych;</li>
	<li>aktywnie realizowanie projektów w oparciu o pozyskane środki w ramach Programu Operacyjnego Kapitał Ludzki, Funduszu Organizacji Pozarządowych oraz innych dostępnych źródeł.</li>
</ul>
<ul>
	<b>W ramach programu prowadzone są takie wydarzenia jak:</b>
	<li>„ASOS – E-senior – szkolenia komputerowe dla osób 60+”- 2013, 2014, 2015</li>
<li>„Frankowicze: społeczno-gospodarcze skutki polskiego boomu kredytowego” – konferencja 2015</li>
<li>„Lepsze Niepołomice” budżet obywatelski 2015</li>
<li>Program „Nie wystarczy być…kobietą” – cykl konferencji prowadzony od 2012 roku, poświęconych sytuacji kobiet w polskim i światowym biznesie, potencjale Kobie - w szczególności tych po czterdziestce, rozwojowi zawodowemu kobiet, wykorzystaniu ich potencjału i doświadczenia życiowego w budowaniu własnej marki</li>
		
			
			
				</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
