<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle "> Nowy Kształt Bezpieczeństwa Euro-Atlantyckiego
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
            Program prowadzony od roku 1995 jest poświęcony zmianom w architekturze bezpieczeństwa zachodzącym w obszarze Euro-Atlantyckim po roku 1989. Podczas konferencji międzynarodowych i seminariów eksperckich omawiane są czynniki określające współczesny model bezpieczeństwa oraz politykę bezpieczeństwa państw obszaru Euro-Atlantyckiego. Panujący obecnie regres w stosunkach pomiędzy niektórymi państwami Europy Zachodniej a Stanami Zjednoczonym oraz zmiany w układzie sił na arenie międzynarodowej, stanowią poważne wyzwanie dla dalszego rozwoju międzynarodowej polityki bezpieczeństwa. <br> 

Z tego powodu podczas odbywających się w ramach programu międzynarodowych konferencji, szczególny nacisk kładziony jest na kwestie umacniania więzi transatlantyckich, podsumowania szczytów NATO, zmian zachodzących w Sojuszu – zarówno wobec krajów członkowskich jak i państw pretendujących do przystąpienia do NATO, zaangażowania Paktu w konflikty regionalne i międzynarodowe, relacje na linii NATO-Rosja oraz wzmocnienie siły militarnej Sojuszu.
<ul>
	<b>Ważniejsze wydarzenia programu "Nowy Kształt Bezpieczeństwa Euro-Atlantyckiego": </b><br>
	<li>konferencja międzynarodowa "Nowy kształt współpracy międzynarodowej USA – EUROPA – ROSJA" – maj 2003</li>
<li>"Dyplomacja w czasie pokoju i wojny" Okrągły Stół z Ambasadorami USA – październik 2004</li>
<li>"Europejska polityka obronności: wiek Entente Cordiale" Seminarium polsko-brytyjsko-francuskie oraz wizyta Księcia Edwarda – listopad 2004</li>
<li>konferencja międzynarodowa "Europejski Program Badań nad bezpieczeństwem (ESRP) szansą dla wzrostu innowacyjności w Europie" – sierpień 2005</li>
<li>wykład dyrektora kursu informacyjnego dla kadry NATO w waszyngtońskiej Akademii Obrony Narodowej Lawrence'a R. Chalmera – grudzień 2007</li>
<li>Forum Bezpieczeństwa Euroatlantyckiego „Nowy kształt bezpieczeństwa euroatlantyckiego – nowa odpowiedzialność” – październik 2008</li>
<li>II Forum Bezpieczeństwa Euroatlantyckiego „NATO przed szczytem jubileuszowym. Czy Sojusz potrzebuje Nowej Koncepcji Strategicznej?” – luty 2009</li>
<li>przygotowanie przez zespół analityków zaproszonych do współpracy przez Instytut Stosunków Międzynarodowych UW oraz krakowską Fundację Instytut Studiów Strategicznych propozycji Nowej Koncepcji Strategicznej Paktu Północnoatlantyckiego oraz uroczyste przekazanie jej 12 marca 2010 Sekretarzowi Generalnemu NATO Andersowi Fogh Rasmussenowi.
<li>IV Forum Bezpieczeństwa Euroatlantyckiego „NATO po przyjęciu Nowej Koncepcji Strategicznej”, konferencja z udziałem Prezydenta RP Bronisława Komorowskiego oraz Sekretarza Generalnego NATO Andersa Fogh Rasmussena - marzec 2011</li>
<li>Jubileuszowa XXV Międzynarodowa Krakowska Konferencja Bezpieczeństwa „ Po szczycie NATO w Warszawie – właściwa odpowiedź na ciężkie czasy?” – październik 2016</li>
<li>XXVI Międzynarodowa Krakowska Konferencja Bezpieczeństwa „Ameryka i bezpieczeństwo euroatlantyckie – ustanawiając nowy porządek?” – marzec 2017</li>

			
			
				</ul>
				</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
