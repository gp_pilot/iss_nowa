<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Zarząd ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Zarząd ISS</h1>
            	<h2>Skład Zarządu Fundacji Instytut Studiów Strategicznych</h2>
            
            </div>
        </div>
    </section>
     <section class="container"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Bieżącą działalnością Fundacji Instytut Studiów Strategicznych kieruje dyrektor, wspierany przez Zarząd ISS.                 	<!--	<span>2016-07-31</span> -->
              	 </header>
            	

    <ul class=" my-content">
		<li>Anna Szymańska-Klich – Prezes Zarządu</li>
		<li>Antoni Potocki</li>
		<li>Andrzej Zdebski</li>
		<li>Prof. dr hab. Zdzisław Mach</li>
		<li> Prof. dr hab. Grażyna Skąpska</li>
		<li>Prof. UJ, dr hab. Artur Gruszczak</li>
		<li>Prof. UEK, dr hab. Piotr Buła</li>
		<li>Rafał Sonik</li>
		<li>Urszula Gacek</li>
		<li>Wojciech Wróblewski</li>
	</ul>

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
