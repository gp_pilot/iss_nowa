<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Instytut Studiów Strategicznych</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="shortcut icon" href="img/icons/favicon.ico" />
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
   
  
  </head>
  <body>
   <header id="menu" class="navbar-fixed-top">
  	<div class="container-fluid">
   
    	<div class="col-xs-12">
                	<nav class="navbar navbar-inverse" role="navigation" id="pasek_nawi">
                    	<div class="container-fluid">
                        	<div class="navbar-header">
                            	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu_zwijane">
                                	<span class="sr-only">Nawigacja</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                           
                            </div>
                            <div class="collapse navbar-collapse" id="menu_zwijane">
                            	<ul class="nav navbar-nav navbar-right">
                                	<li class="active"><a href="#">home</a></li>
                                    <li><a href="#aktualnosci">aktualności</a></li>
                                    <li><a href="#konferencje">konferencje</a></li>
                                    <li><a href="#programy">programy</a></li>                               
                                    <li><a href="#wydawnictwo">wydawnictwo</a></li>
                                    <li><a href="#o_instytucie_content">o instytucie</a></li>
                                    <li><a href="#kontakt">kontakt</a></li>
                                    
                                </ul>
                            </div>
                        </div>
                    </nav>
         </div>
    </div>
  </header>
  <div class="container-fluid">
    	<!--<div class="row top big">
            <div class="container">
        	 	<div class="col-md-8">
             		<div class="btn-group" role="group" aria-label="...">
                	
                      <button type="button" class="btn btn-default">Newsletter</button>
                      <button type="button" class="btn btn-default">Wersja kontrastowa</button>
               		 </div>
             	</div>
    		 	<div class="col-md-4 ">
             		<div class="language">
                          <span> Wybierz język:</span>
                          	<a href="#"><div class="en">EN</div></a>
                            <a href="#"><div class="pl">PL</div></a>
                 	</div>
            	</div>
       	 	</div>
       	 	-->
    </div>
         <div class="container brand">
          	<div class="row">
            	<div class="col-md-3 logo">
                	 <a href="#"><img src="img/logo.png" class="img-responsive"></a>
                </div>
                <div class="col-md-6">
                	<p class="info"><span>Fundacja  Instytut Studiów Strategicznych</span> jest specjalistyczną  jednostką badawczą podejmującą najważniejsze problemy Polski i współczesnego świata. Obszarem działalności instytutu są stosunki międzynarodowe ze szczególnym uwzględnieniem integracji Polski z Unią Europejską
                    </p>
                </div>
                <div class="col-md-3">
                	<a href="https://www.facebook.com/ISSCracow/"><div class="face"></div></a>
                    <a href="https://twitter.com/fundacjaiss"><div class="tweet"></div></a><div class="btn-group" role="group" aria-label="...">
                	
                      <!--<button type="button" class="btn btn-default">Newsletter</button>-->
                      <!--<button type="button" class="btn btn-default">Wersja kontrastowa</button>-->
               		 </div>
                    <!--
                    <div class="dropdown">
                    	<a  data-toggle="dropdown" data-target="#" href="#"><i class="fa fa-bars fa-2x menu-top-button" aria-hidden="true" ></i></a>
                     
                            <ul class="dropdown-menu">
                            	<li><a href="">AKTUALNOŚCI</a></li>
                                <li><a href="">WYDAWNICTWO</a></li>
                                <li><a href="">PROGRAMY</a></li>
                                <li><a href=""> O INSTYTUCIE</a></li>
                                <li><a href="">KONTAKT</a></li>
    
                            </ul>
                       
                    </div>
                    -->
                </div>
               
                	
        
            </div>
         </div>
       
         <div id="glowna" class="carousel slide" data-ride="carousel" data-interval="5000">
              <!--
              <ol class="carousel-indicators">
                <li data-target="#glowna" data-slide-to="0" class="active"></li>
                <li data-target="#glowna" data-slide-to="1"></li>
                <li data-target="#glowna" data-slide-to="2"></li>
              </ol>
            -->
              <div class="carousel-inner">
               <div class="item active">
                  <a href="#"><img src="img/gallery/17_04_2018_1.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>konferencja "Pomiędzy NATO a Unią Europejską – wspólne bezpieczeństwo i wyzwania” </h3>
                    
                  </div>
			   </div>
               <div class="item">
                  <a href="#"><img src="img/gallery/17_04_2018_2.jpg" class="img-responsive"/></a>
					  <div class="carousel-caption">
						<h3>konferencja "Pomiędzy NATO a Unią Europejską – wspólne bezpieczeństwo i wyzwania” </h3>

					  </div>
			   </div>
			 
               <div class="item">
                  <a href="#"><img src="img/gallery/conference.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>SAVE THE DATE: XXVII INTERNATIONAL SECURITY CONFERENCE "BETWEEN NATO AND EUROPEAN UION - COMMON SECURITY AND CHALLENGES" April 16, 2018 Krzysztofory Palace, Cracow, Poland</h3>
                    
                  </div>
				  </div>
               <div class="item ">
                  <a href="#"><img src="news/04-02-2018/1.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>"Dialog Obywatelski z Maciejem Popowskim" </h3>
                    
                  </div>
				  </div>
                <div class="item ">
                  <a href="#"><img src="news/22-09-2017/2.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>"Dialog Obywatelski z Komisarz Elżbietą Bieńkowską" </h3>
                    
                  </div>
                </div>
                 <div class="item">
                   <a href="#"><img src="news/22-09-2017/3.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                   <h3>"Dialog Obywatelski z Komisarz Elżbietą Bieńkowską" </h3>
                    
                  </div>
                </div>
                 <div class="item">
                  <a href="#"><img src="news/22-09-2017/4.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>"Dialog Obywatelski z Komisarz Elżbietą Bieńkowską" </h3>
                    
                  </div>
                </div>
                <div class="item ">
                  <a href="#"><img src="img/gallery/konf1.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>XXIII międzynarodowa krakowska konferencja bezpieczeństwa <br>pt. „NATO – ROSJA” </h3>
                    
                  </div>
                </div>
                <div class="item">
                  <a href="#"><img src="img/gallery/nato1.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                     <h3> XXVI Międzynarodowa Krakowska Konferencja Bezpieczeństwa "Ameryka i bezpieczeństwo euroatlantyckie <br>- ustanawiając nowy porządek?"</h3>
                   
                  </div>
                </div>
                <div class="item">
                  <a href="#"><img src="img/gallery/startup_4.jpg" class="img-responsive"/></a>
                  <div class="carousel-caption">
                    <h3>Konferencja pt"Co to jest startup i dlaczego jest dla wszystkich"</h3>
                    
                  </div>
                </div>
              </div>
            
              <a class="left carousel-control" href="#glowna" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
              </a>
              <a class="right carousel-control" href="#glowna" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
              </a>
        </div>
    	<div  id="about" class="container-fluid">
        	<div class="row">
            	<div class="col-xs-12">
            		<h1 class="text-center tittle"><span>Fundacja</span> Instytut Studiów Strategicznych</h1>
                </div>
            </div>
            <div class="row patern">
                <div class="container">
                    <div class="col-md-4 text-center">
                        <div class="circle center-block">
                            <i class="fa fa-users fa-5x" aria-hidden="true"></i>
                         </div>
                        
                         <h2>KONFERENCJE</h2>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="circle center-block">
                            <i class="fa fa-comments-o fa-5x" aria-hidden="true"></i>
                         </div>
                         <h2>PROGRAMY</h2>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="circle center-block">
                            <i class="fa fa-file-text-o fa-5x" aria-hidden="true"></i>
                         </div>
    
                        <h2>WYDAWNICTWO</h2>
                    </div>
                 </div>
            </div>
        </div>
    	<div class="container">
       		<div class="row">
        	  <div id="aktualnosci">
				<h2>Aktualności</h2>
                <div id="aktualnosci-slide" class="carousel slide" data-ride="carousel" data-interval="50000" data-pause="hover">
              <!--
              <ol class="carousel-indicators">
                <li data-target="#glowna" data-slide-to="0" class="active"></li>
                <li data-target="#glowna" data-slide-to="1"></li>
                <li data-target="#glowna" data-slide-to="2"></li>
              </ol>
            -->
				  <div class="carousel-inner">
					<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					
						

						<?php
					  		include_once('aktualnosci.php');
					  	?>
					
				 
				 
				 
				 
				 
					 </div>
					 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
						<span class="glyphicon glyphicon-chevron-left"></span>
					 </a>
					 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
						<span class="glyphicon glyphicon-chevron-right"></span>
					</a>
				 
            	</div>
            </div>
		  </div>
      
          <div class="row">
          <div id="konferencje">
				<div id="konferencje_content" class="col-xs-12">
				
					<div class="box"> <i class="fa fa-users fa-4x center-block" aria-hidden="true"></i></div>
					<h2>Konferencje międzynarodowe</h2>
					<p>
					Cykl konferencji międzynarodowych zainaugurowało w 1993 roku spotkanie "Nowe wyzwania - nowa odpowiedzialność" poświęcone współpracy polsko-ukraińskiej. O znaczeniu tej sesji świadczą słowa ówczesnego ambasadora Ukrainy, Gennadija Udowenki, który uznał, że była ona jednym z trzech najistotniejszych wydarzeń w pierwszym etapie nowych kontaktów polsko-ukraińskich. Podczas następnych konferencji międzynarodowych, poświęconych kolejno współpracy wyszehradzkiej, stosunkom polsko-rosyjskim, integracji Polski z Unią Europejską oraz problemom bezpieczeństwa międzynarodowego próbowano oceniać stosunki naszego kraju z sąsiadami i organizacjami międzynarodowymi, a także proponować rozwiązania na przyszłość. Inne konferencje międzynarodowe miały charakter tematyczny i dotyczyły problematyki wspólnej dla wszystkich krajów postkomunistycznych. "Wolne media dla Europy", "Konstytucja w służbie demokracji", "Ekologia, demokracja, wolny rynek" przygotowywane były z myślą, by porównywać doświadczenia, opisywać bariery w procesie transformacji oraz poszukiwać sposobów ich przezwyciężania. Szczególne znaczenie miała konferencja "Od komunizmu do demokracji" z czerwca 1995 roku, która stanowiła swoiste podsumowanie głównych tendencji występujących podczas reform ustrojowych, tworzenia zrębów wolnego rynku oraz społeczeństwa obywatelskiego w krajach Europy Środkowej i Wschodniej. Zgromadziła ona kilkudziesięciu liderów politycznych, ekonomistów i działaczy społecznych, którzy w owym czasie nadawali kierunek przemianom w swoich krajach.
					</p>
				</div>
			  </div>
			</div>
	   
        
		<div class="row row-margin">
			<div id="programy">
				<div id="programy_content" class="col-md-8">
					<div class="box"> <i class="fa fa-comments-o fa-4x center-block" aria-hidden="true"></i></div>
					<h2>Programy edukacyjne i badawcze</h2>
					<p>
						Fundacja Instytut Studiów Strategicznych prowadzi programy badawcze, w ramach których organizowane są projekty: międzynarodowe konferencje, seminaria eksperckie, debaty publiczne, warsztaty, szkolenia i inne:
					</p>
					<ul>
						<a href="polska_w_unii_europejskiej.php"><li>od 1994 roku: Polska w Unii Europejskiej</li></a>
						<a a href="nowy_ksztalt.php"><li>od 1995 roku: Nowu Kształt Bezpieczeństwa Euro-Atlantyckiego</li></a>
						<a href="polska_polityka_wschodnia.php"><li>od 1996 roku: Polska polityka wschodnia</li></a>
						<a a href="program_iss_dla_ngo.php"><li>od 2000 roku: Program ISS dla NGO</li></a>
						<a href="program_index.php"><li>od 2005 roku: Program INDEX Pamięci Polaków Zamordowanych
							i Represjonowanych przez Hitlerowców <br>za Pomoc 
							Żydom (zakończony w 2008 r.)</li></a>
						<a href="sprawiedliwi.php"><li>od 2009 roku: Program Sprawiedliwi i ich świat - spotkania z historią dla młodzieży ponadgimnazjalnej</li></a>



					</ul>


				</div>
			</div>
			<div id="slady" class="col-md-4">
				<h2>Śladami kultury i historii żydów</h2>
				<ul>
					<a href="http://zydziwmalopolsce.pl"><li>Żydzi w Małopolsce</li></a>
					<a href="http://www.ksiega-sprawiedliwych.pl/"><li>Księga Sprawiedliwych Wśród Narodów Świata</li></a>
				</ul>
		    </div>
	  </div>
	  	<!-- /////////////////////////////////////WYDAWNICTWO////////  -->
 
	  		<div class="row">
	  		<div id="wydawnictwo">
				<div id="wydawnictwo_content" class="col-md-8">
				
									
				          <div class="box"><i class="fa fa-file-text-o fa-4x center-block" aria-hidden="true"></i></div>
		               	  <h2>Wydawnictwo</h2>
			                <div id="wydawnictwo-slide" class="carousel slide" data-ride="carousel" data-interval="5000" data-pause="hover">
			                
			                          					
								  <div class="carousel-inner">
									<?php
					  					include_once('wydawnictwo.php');
					  				?>
									


									 </div>
									 
							 
            					</div>
					
				</div>
			</div>
			
			<div id="wspolpraca" class="col-md-4">
				<h2>Współpraca</h2>
				<h3>Wydawnictwo Fundacji Instytut Studiów Strategicznych <br>zaprasza do współpracy:</h3>
			  <address>Michał Burek<br>
			  			ul. Mikołajska 4 Pierwsze Piętro<br>
						31-027 Kraków<br>
				  <a href="mailto:wydawnictwo@iss.krakow.pl"> <i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i> <b>wydawnictwo@iss.krakow.pl</b></a>
				  
				  <span><i class="fa fa-phone fa-lg" aria-hidden="true"></i> <b>(+48) 012 421 97 04</b></span>

			  			
			  </address>	
				
				
			</div>
		
			
		</div>
	  <div class="row">
	  <div id="o_instytucie_content">
	  <div class="o_instytucie">
			<div id="o_instytucie" class="col-md-4">
				<div class="logo_iss img-responsive"><img src="img/logo.png"></div>
				<h2>O instytucie</h2>
				<!--<p>
					Fundacja Instytut Studiów Strategicznych prowadzi programy badawcze, w ramach których organizowane są projekty: międzynarodowe konferencje, seminaria eksperckie, debaty publiczne, warsztaty, szkolenia i inne:
				</p> -->
				<ul>
					<a href="zarzad.php"><li>Zarząd</li></a>
					<!--<a><li>Nagroda ISS</li></a> 
					<a><li>Partnerzy Instytutu</li></a>-->
					<!--<a href="sponsorzy.php"><li>Sponsorzy</li></a>-->
					<a href="sprawozdania.php"><li>Sprawozdania z działalnosci</li></a>
					<a href="documents/statut.pdf" target="_blank"><li>Statut ISS</li></a>
					<a><li>Media o nas</li></a>
					<a href="documents/folder_informacyjny/folder_pl.pdf"><li> Folder informacyjny</li></a>
				</ul>
				
				
			</div>
		  </div>
			<div id="historia" class="col-md-8">
				<h2>HISTORIA ISS</h2>
				<p>
					Początki Fundacji Instytut Studiów Strategicznych sięgają 1993 roku, kiedy grupa entuzjastów przekonanych o potrzebie społecznego wsparcia instytucji demokratycznego państwa powołała do życia Międzynarodowe Centrum Rozwoju Demokracji. Ich zamierzeniem było stworzenie ośrodka, który ułatwiałby dialog między przedstawicielami różnych orientacji politycznych oraz służył wymianie doświadczeń między politykami z różnych krajów. Miał także za zadanie ułatwiać komunikację między ekspertami a elitami politycznymi, tak by wiedza tych pierwszych mogła zostać lepiej wykorzystana podczas podejmowania decyzji politycznych. Z oczywistych względów musiała to być instytucja pozarządowa, nie związana ani z partiami politycznymi, ani z administracją rządową.
Idea stworzenia forum debaty publicznej, które umożliwiałoby ścieranie się różnych poglądów i wypracowanie rozwiązań korzystnych dla kraju spotkała się z silnym poparciem – założycielami fundacji były Uniwersytet Jagielloński oraz Akademia Ekonomiczna (dziś Uniwersytet Ekonomiczny). Natomiast Radę Programową stworzyli: Władysław Bartoszewski, Leszek Balcerowicz, Wiesław Chrzanowski, Bronisław Geremek, Marian Grzybowski, Aleksander Hall, Jerzy Jedlicki, Krzysztof Kozłowski, Józef Lassota, Jan Małecki, Jerzy Mikułowski-Pomorski, Jerzy Milewski, Jan Nowak-Jeziorański, Andrzej Olechowski, Andrzej Pelczar, Zbigniew Pucek, Paweł Sarnecki, Jacek Saryusz-Wolski, Zygmunt Skórzyński, Krzysztof Skubiszewski, Jerzy Turowicz, Edward Wende, Edmund Wnuk-Lipiński, Andrzej Zakrzewski i Janusz Ziółkowski. Prezesem Fundacji został Bogdan Klich.
"Fundacja ma refleks, jest aktywna, twórcza, podejmuje we właściwym momencie tematy o kluczowym znaczeniu. Jest to znakomita wizytówka polskiego myślenia, polskiej debaty: i politycznej i profesjonalnej w różnych dziedzinach" – mówił o Fundacji Jacek Saryusz-Wolski, ówczesny Pełnomocnik Rządu d/s Integracji Europejskiej. Jan Nowak-Jeziorański dodawał: "MCRD jest dla mnie imponującym symptomem polskiego dynamizmu i rozmachu, który został wyzwolony w roku 1989. Jest owocem odzyskanej wolności. Podziwiam, jak wiele tu stworzono, dosłownie z niczego."
				</p>
		    </div>
	  </div>
	</div>
</div> 
<div class="container-fluid" id="contact" >
	
		<div class="row">
			<div class="col-md-4">
			<div class="col-md-4">
				</div>
			<div class="col-md-8">
				<address>
					KONTAKT:<br><br>
					Fundacja Instytut Studiów Strategicznych<br>
					ul. Mikołajska 4,<br>
					31-027 Kraków,<br>
					tel./fax: +48 12 421 62 50<br>
					<a href="mailto:biuro@iss.krakow.pl">biuro@iss.krakow.pl</a><br>
					NIP: 676-01-02-235<br>
					Sekretariat czynny:<br>
					od poniedziałku do piątku <br>od godz. 8.30 do godz. 16.30

				</address>
				</div>
			</div>
		
			<div id="map" class="col-md-8">
				
			</div>
		</div>
		<div class="row">
			<footer class="col-xs-12 text-center" >
				<p>copyright© 2017 INSTYTUT STUDIÓW STRATEGICZNYCH</p>
			</footer>
		</div>
	
</div>	
	
	
<!--MAPA--> 
         
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="//maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>
        <script src="js/gmap3.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCYI6CJajUeR8w51NGQ5TLX6EgBSy7-PUc"></script>
        
<script type="text/javascript">
	$('#map').gmap3({
    map: {
        options: {
            center: [50.061402, 19.941382],
            zoom: 15,
            panControl: false,
            zoomControl: false
        }
    },
    marker: {
        values:[{
            latLng: [50.061402, 19.941382],
            data: 'ISS'
        }],
        events:{
            click: function(marker, event, context) {
                var map = $(this).gmap3('get'),
                    infowindow = $(this).gmap3({get:{name:'infowindow'}});
                if (infowindow) {
                    infowindow.open(map, marker);
                    infowindow.setContent(context.data);
                } else {
                    $(this).gmap3({
                        infowindow: {
                            anchor: marker,
                            options: {content: context.data}
                        }
                    });
                }
            }
        }
    }
});
</script>
    
  <!--MAPA-->   
    <script src="js/wlasny.js"></script> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    
    
  </body>
</html>