<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->	
		<div class="item active">
						<div class="row aktualnosc-bg">
							<p class="date">24-06-2020</p>
							<div class="col-md-4">
								<a href="https://www.youtube.com/watch?v=srVuV0bob2o&feature=youtu.be" target="blank"><img src="../news/24-06-2020/zaproszenie.jpg" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2 style="line-height:2.4rem">
								 „Plan odbudowy Unii po pandemii Covid 19 <br>– nowe pieniądze, nowe możliwości” <br>- Debata z europosłem Janem Olbrychtem.
								</h2>
								<p>
								Zaproszenie do udziału w debacie online:<br>
									
									Link do debaty <a href="https://www.youtube.com/watch?v=srVuV0bob2o&feature=youtu.be" target="blank">YouTube</a><br>
								
									Link do debaty Facebook <a href="https://www.facebook.com/events/404311207191434/" target="blank">Facebook</a><br><br>
									

Bardzo prosimy o potwierdzenie uczestnictwa: <br><br>
E-mail: biuro@iss.krakow.pl<br>
<br><br>
									Co dalej po pandemii? Debata na temat unijnej pomocy dla państw wspólnoty<br><br>

„Plan odbudowy Unii po pandemii Covid 19 – nowe pieniądze, nowe możliwości” to temat wirtualnego spotkania z eurodeputowanym Janem Olbrychtem organizowanego przez Przedstawicielstwo Komisji Europejskiej w Polsce i Fundację Instytut Studiów Strategicznych. Debata odbędzie się 1 lipca 2020 roku o godz. 16.00.<br><br>

Celem rozmowy jest przybliżenie uczestnikom  idei, którą kierowała się Komisja Europejska przygotowując Next Generation UE – program wspierający wysiłki krajów członkowskich w zażegnaniu kryzysu wywołanego pandemią, a także pobudzeniu gospodarki unijnej. Finansowe wsparcie w ramach NGEU w wysokości 750 mld euro pozwoli zwiększyć finansowe możliwości budżetu UE na lata 2021 -2027 do 1,85 bln euro. Projekt przewiduje inwestycje w kluczowe dla rozwoju UE działania ekologiczne i cyfrowe jako siłę napędową dla wzrostu gospodarczego państw członkowskich.<br><br>

Europoseł Jan Olbrycht w obecnej kadencji Parlamentu Europejskiego jest wiceprzewodniczącym Grupy Europejskiej Partii Ludowej, członkiem komisji ds. budżetu a także zastępcą szefa komisji ds. rozwoju regionalnego i komisji ds. kontroli budżetowej. Jest także jednym z koordynatorów w komisji ds. budżetu z ramienia Europejskiej Partii Ludowej (EPL/EPP) oraz stałym sprawozdawcą Parlamentu Europejskiego do spraw wieloletnich ram finansowych  (WRF/ MFF). W obszarze jego działań jest także koordynowanie prac grypy roboczej ds. dialogu międzykulturowego i religijnego oraz kierowanie Intergrupą URBAN.<br><br>

Wirtualne spotkanie z posłem Janem Olbrychtem odbędzie się 1 lipca 2020 roku o godz. 16.00. Poprowadzi je Anna Szymańska-Klich, prezes Fundacji Instytut Studiów Strategicznych.


					    			
					    		
						    	
								
									
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>


<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->	
		<div class="item ">
						<div class="row aktualnosc-bg">
							<p class="date">06-06-2019</p>
							<div class="col-md-4">
								<img src="news/06-06-2019/program.jpg" alt="ISS" class="img-responsive">
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								Europejski Kraków - 15 lat w Unii Europejskiej
								</h2>
								<p>
								Jacek Majchrowski, Prezydent Miasta Krakowa<br>
									oraz<br>
								Fundacja Instytut Studiów Strategicznych<br>

										zapraszają na debatę<br>

										zatytułowaną:<br>

									<b>Europejski Kraków - 15 lat w Unii Europejskiej.</b><br>

									Debata odbędzie się <b>12 czerwca 2019 roku</b><br>

									<b>Sala Miedziana, Pałac Krzysztofory</b><br>

									<b>Rynek Główny 35, Kraków</b><br>
										Rozpoczęcie zaplanowano na godzinę 17:00.
										<br><br>

								
										Wstęp na debatę jest bezpłatny<br>

Serdecznie prosimy o potwierdzenie przybycia do dnia 10 czerwca 2019 <br>

					    			Ilość miejsc ograniczona.<br>

Telefon: (12) 421 62 50<br>

E-mail: biuro@iss.krakow.pl<br>


					    			
					    		
						    	
									<a href="news/06-06-2019/program2.jpg" target="_blank">pobierz program</a>	<br>
									
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>


<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->

					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">06-05-2019</p>
							<div class="col-md-4">
								<a href="news/06-05-2019/001.jpg"><img src="news/06-05-2019/info.jpg" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<b>DIALOG OBYWATELSKI</b>
								<h2>
								
									Z UDZIAŁEM <b>KOMISARZ UE ELŻBIETY BIEŃKOWSKIEJ</b>
								</h2>
								<p>
							
							13 maja 2019 godz. 11:30-13:00<br><br>
							Międzynarodowe Centrum Kultury<br>
							 Rynek Główny 25<br>
									
										31-011 Kraków<br><br>
									Zapisy od 8 maja pod adresem:<br>
								dialog@iss.krakow.pl<br>
									lub tel:. +48 12 421 62 50<br>
									Liczba miejsc ograniczona
					    			
					    			
					    		
						    	
								
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="https://www.nato.int/"><img src="img/logotypy/nato2.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="http://www.kas.de/polen/pl/"><img src="img/logotypy/adenauer.jpg" /></a></li>
									 <!--<li  style="padding-right:60px;"><a href="http://www.marr.pl/"><img src="img/logotypy/marr.jpg" /></a></li> -->
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->		
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">14-03-2019</p>
							<div class="col-md-4">
								<a href="news/14-03-2019/zaproszenie.pdf"><img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<b>XXVIII Międzynarodowa Krakowska Konferencja Bezpieczeństwa</b>
								<h2>
								
									Czy NATO sprosta nowym wyzwaniom bezpieczeństwa?"
								</h2>
								<p>
							Konferencja organizowana jest przez<br>
							Fundację Instytut Studiów Strategicznych<br>
								we współpracy <br>
							z Kwaterą Główną NATO oraz Fundacją Konrada Adenauera w Polsce<br><br>
							22 marca 2019<br><br>
							„Sala Miedziana”<br>
								Pałac Krzysztofory, Rynek Główny 35<br>
									
										31-011 Kraków<br><br>
					    			
					    			
					    		
						    	
									<a href="news/14-03-2019/Agenda_PL.pdf">pobierz program PL</a>	<br>
									<a href="news/14-03-2019/Agenda_ENG.pdf">download program ENG</a>
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="https://www.nato.int/"><img src="img/logotypy/nato2.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="http://www.kas.de/polen/pl/"><img src="img/logotypy/adenauer.jpg" /></a></li>
									 <!--<li  style="padding-right:60px;"><a href="http://www.marr.pl/"><img src="img/logotypy/marr.jpg" /></a></li> -->
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->	


<div class="item ">
						<div class="row aktualnosc-bg">
							<p class="date">21-01-2019</p>
							<div class="col-md-4">
								<img src="news/21-01-2019/zaproszenie.jpg" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								 Dialog Obywatelski "Przyszłość Budżetu UE po 2020 - spójność czy elastyczność?"
								</h2>
								<p>
 Szanowni Państwo,
<br><br>
Biuro Parlamentu Europejskiego w Polsce oraz Przedstawicielstwo Komisji Europejskiej w Polsce zapraszają na Dialog Obywatelski "Przyszłość Budżetu UE po 2020 - spójność czy elastyczność?", który odbędzie się 28 stycznia o godz. 11:00 w Międzynarodowym Centrum Kultury w Krakowie.
<br><br>
Program:
<br>
Otwarcie: Jacek Safuta, Dyrektora Biura Parlamentu Europejskiego w Polsce.<br><br>

11.00-11.15 wystąpienie wprowadzające przedstawiciela Komisji Europejskiej:<br>

                        -Silvano Presa  - Zastępca Dyrektora Generalnego DG Budżet w Komisji Europejskiej<br><br>

11.15-12.30 dyskusja z udziałem posłów do Parlamentu Europejskiego:<br>

                        -Jan Olbrycht (EPP) - współsprawozdawca w sprawie Wieloletnich Ram Finansowych UE 2021-2027<br>

                        -Isabelle Thomas (S&D) - współsprawozdawczyni w sprawie Wieloletnich Ram Finansowych UE 2021-2027<br>
                         Moderator: redaktor Maciej Zakrocki, TOK FM<br><br>

12.30-13.15 sesja pytań i odpowiedzi z udziałem panelistów oraz przedstawiciela KE<br>
Podczas spotkania zapewniamy tłumaczenie symultaniczne.
<br><br>
Z powodu ograniczonej liczby miejsc, osoby zainteresowane prosimy o potwierdzenie uczestnictwa do dnia 24 stycznia: biuro@iss.krakow.pl lub 12 421 62 50.
<br><br>
Zapraszamy!<br><br><br>
									Dane osobowe uzyskane przez Fundację Instytut Studiów Strategicznych są przetwarzane zgodnie z przepisami zawartymi w Rozporządzeniu (WE) nr 2018/1725.
				
									
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								  
							 </div>
						 </div>
						

					   </div>
					
<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">10-11-2018</p>
							<div class="col-md-4">
								<a href="news/06-04-2018/zaproszenie.pdf"><img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								 Oświadczenie Zarządu Instytutu Studiów Strategicznych
								</h2>
								<p>
Zarząd Instytutu Studiów Strategicznych wyraża głębokie oburzenie z powodu niegodnej wypowiedzi godzącej w pamięć śp. Prof. Władysława Bartoszewskiego, wieloletniego przewodniczącego Rady Programowej Instytutu, która padła na antenie Radiowej Trójki w dn. 7.11.2018 w programie „Trójka na poważnie” prowadzonym przez red. Wojciecha Reszczyńskiego.   <br> <br>

To obrzydliwe sformułowanie godzi w pamięć jednego z najbardziej zasłużonych Polaków, który całe swoje życie służył Polakom i Polsce.  Prof. Władysław Bartoszewski, więzień Auschwitz, uczestnik Powstania Warszawskiego, więziony w czasach komunistycznych, odznaczony medalem Sprawiedliwy Wśród Narodów Świata, Minister Spraw Zagranicznych w latach 1995 i 2000-2001, Przewodniczący Międzynarodowej Rady Oświęcimskiej był i jest wzorem polskiego patrioty i zasługuje na najwyższy szacunek i uznanie.
 <br> <br>
W związku z niegodną wypowiedzią dotyczącą osoby śp. Prof. Władysława Bartoszewskiego domagamy się przeprosin za te słowa. Zaś wszystkim przypominamy, cytując za Prof. Władysławem Bartoszewskim, „Warto być przyzwoitym!”. <br> <br>

Zarząd Instytutu Studiów Strategicznych <br>
Anna Szymańska-Klich, Prezes Zarządu<br><br><br>
									
									
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								  
							 </div>
						 </div>
						

					   </div>
				
											<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->																									<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">03-09-2018</p>
							<div class="col-md-4">
								<a href="news/06-04-2018/zaproszenie.pdf"><img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								 Projekt:  Sprawiedliwi i ich świat - spotkania z historią
								</h2>
								<p>
Fundacja Instytut Studiów Strategicznych rozpoczyna realizację projektu  „Sprawiedliwi i ich świat – spotkania z historią” realizowanego w ramach programu „Patriotyzm jutra”, prowadzonego przez Muzeum Historii Polski.  <br> <br>

Projekt „Sprawiedliwi i ich świat” jest skierowany do młodzieży szkół ponadgimnazjalnych województwa małopolskiego. Głównym założeniem projektu jest przybliżenie młodzieży polskich obywateli uhonorowanych tytułem Sprawiedliwy wśród Narodów Świata. Poprzez historię ich życia oraz postawy młodzi ludzie poznają pozytywne wzorce zachowań i wartości, które należy pielęgnować. Drugim celem jest ukazanie Małopolski jako obszaru ukształtowanego na przestrzeni wieków, poprzez wzajemne przenikanie się kultur i ich dialogu, których dziedzictwo przechowywane jest po dzień dzisiejszy. Umiejscowienie danej opowieści w szerszym kontekście historycznym pozwoli młodzieży lepiej poznać historię Żydów polskich oraz okres okupacji niemieckiej. Projekt obejmuje realizację 17 warsztatów z młodzieżą w ich szkole, w grupach 20-osobowych, które prowadzone są w oparciu o przygotowany wcześniej scenariusz prowadzonych zajęć.
 <br> <br>
W ramach dotychczasowych edycji projektu (w latach 2008 – 2017) Instytut zorganizował spotkania dla ponad 11350 uczniów szkół ponadgimnazjalnych w Małopolsce. 
<br>
Zainteresowane uczestnictwem w projekcie  szkoły prosimy o kontakt: <br> biuro@iss.krakow.pl, Tel./fax. 12 421 62 50
									
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									
									<li style="padding-right:60px">
										<a href="http://muzhp.pl/pl/"><img src="img/logotypy/mhp.jpg"></a>
									
									 </li>
									 	<li style="padding-right:60px">
										<a href="http://muzhp.pl/pl/c/1532/patriotyzm-jutra"><img src="img/logotypy/patriotyzm_jutra.jpg"></a>
									
									 </li>


								 </ul> 
							 </div>
						 </div>

					   </div>
				
											<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->														<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">24-08-2018</p>
							<div class="col-md-4">
								<a href="news/24-08-2018/info.jpg" target="_blank"><img src="news/24-08-2018/info.jpg" alt="konferencja" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								 Dialog Obywatelski "Polska w Unii Europejskiej - wspólne wartości, wspólne wyzwania"
								</h2>
								<p>
Przedstawicielstwo Komisji Europejskiej w Polsce oraz Fundacja Instytut Studiów Strategicznych zapraszają seniorów do udziału w Dialogu Obywatelskim "Polska w Unii Europejskiej - wspólne wartości, wspólne wyzwania" w dniu 1 października 2018 r. w Krakowie, z udziałem Marka Prawdy, Dyrektora Przedstawicielstwa KE w Polsce oraz aktora Olgierda Łukaszewicza. 
Wstęp wolny dla wszystkich chętnych po uprzedniej rejestracji i do wyczerpania miejsc.<br><br>

Dialogi Obywatelskie to inicjatywa Komisji Europejskiej, której celem jest rozbudzenie debaty na tematy europejskie. Spotkania w tej formule odbywają się w krajach UE od 2012. Podczas spotkań przedstawiciele instytucji UE przedstawiają zagadnienia związane z działaniem wspólnoty oraz słuchają głosów, opinii i propozycji obywateli na temat funkcjonowania UE.<br><br>
									Udział w Dialogu z Panem Ambasadorem Markiem Prawdą, Szefem Przedstawicielstwa KE w Polsce, oraz z Panem Olgierdem Łukaszewiczem to doskonała okazja do podyskutowania o bieżących tematach dotyczących Polski w Unii Europejskiej. W części dyskusyjnej uczestnicy, poza zadawaniem pytań, będą mogli również zgłosić ewentualne uwagi, które następnie będą przekazane decydentom w Komisji Europejskiej.
									
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								  
							 </div>
						 </div>
						

					   </div>
				
											<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->																			<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">26-04-2018</p>
							<div class="col-md-4">
								<img src="news/26-04-2018/img.jpg" alt="konferencja" class="img-responsive">
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								Krakowskie Święto Europy
								</h2>
								<p>
							Czy 14 lat to wystarczająco dużo czasu by nauczyć się “Ody do Radości”?
Sprawdźmy to wspólnie 6 maja podczas Święta Unii Europejskiej.
Spotkajmy się na Małym Rynku, by wspólnie przypomnieć sobie radość i 
emocje które towarzyszyły nam 14 lat temu.
W referendum Polacy potwierdzili, że chcą by Unia Europejska stała się 
gwarantem naszych ciężko wywalczonych praw i wolności.
Głosując za Polską w Unii Europejskiej, chcieliśmy wzmocnić szanse na 
rozwój i bezpieczną przyszłość Polski.
Chcieliśmy wejść do Unii, bo równość , wolność ,demokracja, wzajemny 
szacunek i otwartość - nasze wartości - to fundament Unii Europejskiej!<br><br>
		Spotkajmy się 6 maja by pokazać, że Unia Europejska gwarantuje naszą 
pozycję w Europie, że Unia Europejska to My.
Spotkajmy się, by wspólnie świętować!
Spotkajmy się, by wspólnie zaśpiewać “Mazurka Dąbrowskiego”i “Odę do 
Radości”!
<br><br>
Szczegółowy program przedstawimy jeszcze w tym tygodniu. Serdecznie 
zapraszamy wszystkich Demokratów do współpracy przy organizacji Święta.
			
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								  
							 </div>
						 </div>
						

					   </div>
				
											<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">24-04-2018</p>
							<div class="col-md-4">
								<img src="news/24-04-2018/img.jpg" alt="konferencja" class="img-responsive">
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								Fundacja Instytut Studiów Strategicznych dziękuje wszystkim uczestnikom konferencji "Kobiety aktywne w życiu publicznym” 
								</h2>
								<p>
							Fundacja Instytut Studiów Strategicznych dziękuje wszystkim uczestnikom konferencji
"Kobiety aktywne w życiu publicznym” za liczne przybycie. Serdecznie dziękujemy Komisarz Elżbiecie Bieńkowskiej, Dorocie Segdzie, Joannie Bensz, Marcie Lempart, Urszuli Gacek oraz moderator spotkania, Renacie Lisowskiej, za interesujące wypowiedzi, podzielenie się swoją wiedzą zawodową, doświadczeniem życiowym i motywacją do działania. Cieszymy się, że zarówno spotkanie z panelistkami jak i warsztat "Wystąpienia publiczne" prowadzony przez Panią Gacek cieszyły się dużym zainteresowaniem..<br><br>
			
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									
									<li style="padding-right:60px">
										<a href="https://www.malopolska.pl/"><img src="img/logotypy/malopolska.png"></a>
									
									 </li>
									 	<li style="padding-right:60px">
										<a href="https://www.malopolska.pl/"><img src="img/logotypy/gemini.jpg"></a>
									
									 </li>


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					
					<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">17-04-2018</p>
							<div class="col-md-4">
								<img src="news/17-04-2018/1.jpg" alt="konferencja" class="img-responsive">
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								Fundacja Instytut Studiów Strategicznych dziękuje wszystkim uczestnikom konferencji "Pomiędzy NATO a Unią Europejską <br>– wspólne bezpieczeństwo i wyzwania” 
								</h2>
								<p>
								Fundacja Instytut Studiów Strategicznych dziękuje wszystkim uczestnikom konferencji<br> "Pomiędzy NATO a Unią Europejską – wspólne bezpieczeństwo i wyzwania” za liczne przybycie i ciekawą dyskusję. Dziękujemy również panelistom za interesujące wypowiedzi i  podzielenie się swoją wiedzą i doświadczeniem ze wszystkimi gośćmi zebranymi na konferencji.<br><br>

Dziękujemy współorganizatorom konferencji: Kwaterze Głównej NATO i Fundacji Konrada Adenauera w Polsce  oraz  Małopolskiej Agencji Rozwoju Regionalnego, która była sponsorem wydarzenia.<br><br>

Pragniemy przypomnieć, że relacja z konferencji jest dostępna na naszym profilu FB:<br>
									<a href="https://www.facebook.com/ISSCracow/videos/10155245066775974/">I panel</a>,

									<a href="https://www.facebook.com/ISSCracow/videos/10155257037995974/">II panel <a>
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									
									
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
							<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">09-04-2018</p>
							<div class="col-md-4">
								<img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive">
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								"Kobiety aktywne w życiu publicznym" 
								</h2>
								<p>
								Fundacja Instytut Studiów Strategicznych<br>

										zaprasza na VII konferencję z cyklu „Nie wystarczy być... Kobietą"<br>

										zatytułowaną:<br>

									<b>"Kobiety aktywne w życiu publicznym" </b><br>

									Konferencja odbędzie się <b>20 kwietnia 2018 roku</b><br>

									<b>w Hotelu Grand</b><br>

									<b>ul .Sławkowska 5/7, Kraków</b><br>
										Rozpoczęcie i powitanie gości zaplanowano na godzinę 10:20.
										<br><br>

								
										Wstęp na konferencję jest bezpłatny<br>

Serdecznie prosimy o potwierdzenie przybycia do dnia 18 kwietnia <br>

z dopiskiem "konferencja kobieca"<br>
					    			Ilość miejsc ograniczona.<br>

Telefon: (12) 421 62 50<br>

E-mail: biuro@iss.krakow.pl<br>

Projekt realizowany przy wsparciu finansowym Województwa Małopolskiego<br>
					    			
					    		
						    	
									<a href="news/09-04-2018/KOBIETY_2018_Agenda.pdf" target="_blank">pobierz agendę</a>	<br>
									
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="https://www.malopolska.pl/"><img src="img/logotypy/malopolska.png" /></a></li>
									<li style="padding-right:60px">
										<a href="https://www.malopolska.pl/"><img src="img/logotypy/gemini.jpg"></a>
									
									 </li>

									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">06-04-2018</p>
							<div class="col-md-4">
								<a href="news/06-04-2018/zaproszenie.pdf"><img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								Instytut Studiów Strategicznych został jednym z inicjatorów inicjatywy ustawodawczej o ochronie prawnej flagi Unii Europejskiej.
								</h2>
								<p>
								Fundacja Instytut Studiów Strategicznych<br>
został jednym z inicjatorów inicjatywy ustawodawczej<br> o ochronie prawnej flagi Unii Europejskiej.
										<br>

									Razem z Frontem Europejskim rozpoczynamy akcję zbierania podpisów pod<br> obywatelską inicjatywą ustawodawczą o wprowadzeniu ochrony dla flagi UE.

									
										<br>
									Prezentujemy <a href="http://wyborcza.pl/10,82983,23227666,front-europejski-domaga-sie-prawnej-ochrony-unijnej-flagi-ruszyla.html">komunikat prasowy</a> odnośnie inicjatywy wraz <br>z informacjami o ruchu społecznym Front Europejski.
					    			
					    			<br>
					    			<br>
					    		Zapraszamy do składania podpisów<br>
					    		 pod projektem ustawy o fladze Unii Europejskiej.<br>
Podpisy można składać osobiście, w biurze Instytutu Studiów Strategicznych, <br>ul. Mikołajska 4, <br>tuż przy Małym Rynku,<br> od poniedziałku do piątku w godz 9-16.<br>
Zachęcamy też do samodzielnego zbierania podpisów <br>i przekazania nam list, <br>które następnie dostarczymy do siedziby Frontu Europejskiego.<br>
									Formularz oraz ustawa znajdują się na stronie <a href="http://fronteuropejski.pl/">http://fronteuropejski.pl/</a>
						    	<br>
									<a href="news/06-04-2018/Front_Europejski_komunikat_prasowy.pdf" target="_blank">pobierz komunikat</a>	<br>
								
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
					
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">28-03-2018</p>
							<div class="col-md-4">
								<a href="news/08-03-2018/zaproszenie.pdf"><img src="img/aktualnosci/iss.png" alt="ISS" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								„Między NATO a Unią Europejską - wspólne bezpieczeństwo i wyzwania”
								</h2>
								<p>
								Fundacja Instytut Studiów Strategicznych<br>

										we współpracy z<br>

										Kwaterą Główną NATO<br>

										oraz<br>

										Fundacją Konrada Adenauera w Polsce<br>

										 mają zaszczyt zaprosić do udziału w<br>

										XXVII Międzynarodowej Krakowskiej<br>
										Konferencji Bezpieczeństwa<br>
										zatytułowanej:
										<br><br>

									<b>„Między NATO a Unią Europejską - wspólne bezpieczeństwo i wyzwania”</b>

										 <br><br>

										16 kwietnia 2018
										<br>
										Pałac Krzysztofory, Sala Miedziana<br>
										Rynek Główny 35<br>
										31-011 Kraków<br><br>
					    			
					    			
					    		
						    	
									<a href="news/28-03-2018/Agenda_PL.pdf">pobierz program PL</a>	<br>
									 <a href="news/28-03-2018/Agenda_ANG.pdf">download program ENG</a>	
											
					    					
						    					
								</p>
										
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="https://www.nato.int/"><img src="img/logotypy/nato2.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="http://www.kas.de/polen/pl/"><img src="img/logotypy/adenauer.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="http://www.marr.pl/"><img src="img/logotypy/marr.jpg" /></a></li>
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item ">
						<div class="row aktualnosc-bg">
							<p class="date">24-03-2018</p>
							<div class="col-md-4">
								<img src="news/24-03-2018/solidar.jpg" alt="Logo Iss" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" >
								<h2>
									Dzisiaj obchodzimy Narodowy Dzień Pamięci Polaków ratujących Żydów <br>pod okupacją niemiecką. 
								</h2>
								<p>
								
Z tej okazji prezentujemy sylwetki czterech osób nagrodzonych Medalem „Sprawiedliwy wśród Narodów Świata”.
<br><br>
									<a href="news/24-03-2018/news.php"><b>Bartoszewski, Władysław</b></a><br>
Od września 1942 roku, jeszcze przed utworzeniem „Żegoty”, Władysław Bartoszewski był członkiem Tymczasowego Komitetu Pomocy Żydom. Gdy w grudniu 1942 roku utworzono „Żegotę”, Bartoszewski (pseudonim „Ludwik”) całym sercem zaangażował się... <br><br>
									<a href="news/24-03-2018/news.php"><b>Małysiak, Albin<br>
										Wilemska, Bronisława</b><br></a>
Podczas okupacji ksiądz Albin Małysiak i siostra Bronisława Wilemska pomogli pięciorgu Żydom. W tamtym czasie siostra Bronisława kierowała domem dla osób starszych i upośledzonych w Krakowie, gdzie ksiądz Albin był kapelanem... <br><br>
							
									<a href="news/24-03-2018/news.php"><b>Pankiewicz, Tadeusz</b></a><br>
Polski aptekarz, Tadeusz Pankiewicz, przekupił niemieckie władze w Krakowie, żeby móc nadal prowadzić aptekę na terenie getta. Jako farmaceuta, Pankiewicz  oddał się do dyspozycji zamieszkujących getto Żydów... <br>
								
								</p>
									
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
							
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
        <!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">09-03-2018</p>
							<div class="col-md-4">
								<img src="img\aktualnosci\solidar.jpg" alt="Logo Iss" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
									List otwarty organizacji społeczeństwa obywatelskiego
								</h2>
								<p>
								Fundacja Instytut Studiów Strategicznych jest jednym z sygnatariuszy listu „Prawda i pojednanie” wystosowanego przez organizacje pozarządowe do światowych mediów i opinii publicznej w sprawie dialogu polsko-żydowskiego. 
Apel podpisało kilkadziesiąt organizacji społeczeństwa obywatelskiego. <br><br>
"Piszemy do Was z Polski. I z wielu innych miejsc na świecie, gdzie mieszkamy, studiujemy, pracujemy. My, czyli Polki i Polacy, którzy nie godzą się z tym, jak bieżąca polityka kładzie się cieniem na budowanych przez lata relacjach polsko-żydowskich. Do Was, czyli do wszystkich tych, którzy patrzą dziś na Polskę - z niedowierzaniem, smutkiem czy złością. Piszemy, bo zależy nam, żebyście wiedzieli, że niezależnie od tego, jak radykalne i niewłaściwe są stanowiska polskich władz czy niektórych środowisk, nie są to stanowiska i poglądy nas wszystkich"<br><br>
									
								Treść listu dostępna na stronie #Solidarityintruth.
									<a href="https://solidarityintruth.org/contact/">https://solidarityintruth.org/contact/</a>
					
								</p>
							
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="#"><img src="img/logotypy/iss.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="https://ec.europa.eu/commission/index_pl"><img src="img/logotypy/komisja_europejska.jpg" /></a></li>
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
    <!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">04-02-2018</p>
							<div class="col-md-4">
								<img src="news/04-02-2018/1small.jpg" alt="Maciej Popowski" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
									Dialog Obywatelski z Maciejem Popowskim
								</h2>
								<p>
									W dniu 1 lutego w Krakowie odbył się Dialog Obywatelski "Unia Europejska i jej sąsiedztwo - czy mamy się czego obawiać" z udziałem Macieja Popowskiego, Wicedyrektora Dyrekcji Generalnej Komisji Europejskiej, Europejskiej polityki sąsiedztwa i negocjacji w sprawie rozszerzenia (DG NEAR) oraz Senatora Bogdana Klicha. Spotkanie zorganizowane zostało wraz z Przedstawicielstwem Komisji Europejskiej w Polsce. Wszystkim przybyłym dziękujemy za udział w spotkaniu. Tym, którzy nie mieli możliwości uczestniczyć w DO przypominamy, że relacja ze spotkania dostępna jest na kanale <a href="https://www.youtube.com/watch?v=JRWg4s3zwVg"> YouTube </a>
					
								</p>
							
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="#"><img src="img/logotypy/iss.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="https://ec.europa.eu/commission/index_pl"><img src="img/logotypy/komisja_europejska.jpg" /></a></li>
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">05-01-2018</p>
							<div class="col-md-4">
								<a href="news/05-01-2018/zaproszenie.pdf"><img src="news/05-01-2018/zaproszenie.jpg" alt="Prezydent" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								UNIA EUROPEJSKA I JEJ SĄSIEDZTWO - CZY MAMY SIĘ CZEGO OBAWIAĆ?
								</h2>
								<p>
								PRZEDSTAWICIELSTWO KOMISJI EUROPEJSKIEJ W POLSCE ORAZ FUNDACJA INSTYTUT STUDIÓW STRATEGICZNYCH ZAPRASZAJĄ DO UDZIAŁU W
DIALOGU OBYWATELSKIM: <br><b>„UNIA EUROPEJSKA I JEJ SĄSIEDZTWO - CZY MAMY SIĘ CZEGO OBAWIAĆ?”</b><br><br>
GOŚĆ SPECJALNY<br>
									<b>Maciej Popowski </b> <br>
Wicedyrektor Dyrekcji Generalnej Komisji Europejskiej,<br> Europejskiej polityki sąsiedztwa i negocjacji w sprawie rozszerzenia (DG NEAR)<br>
<br><br>
1 lutego; godz. 17:00-18:30; Grand Hotel; ul. Sławkowska 5/7, Kraków<br>
Potwierdzenia przybycia do  29 stycznia:<br> tel. 12 421 62 50; mail: biuro@iss.krakow.pl<br>
<br><b>Liczba miejsc ograniczona.</b> <br> <br>
<a href="news/05-01-2018/Dialog_Obywatelski_opis.pdf">pobierz opis wydarzenia</a>							
								</p>
							
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="#"><img src="img/logotypy/iss.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="https://ec.europa.eu/commission/index_pl"><img src="img/logotypy/komisja_europejska.jpg" /></a></li>
									
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">22-09-2017</p>
							<div class="col-md-4">
								<img src="news/22-09-2017/1.jpg" alt=" " class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
									"Dialog Obywatelski z Komisarz Elżbietą Bieńkowską"
								</h2>
								<p>
								W dniu 8 września w Krakowie odbył się Dialog Obywatelski "Bezpieczniejsza Polska, bezpieczniejsza Europa, bezpieczniejsi obywatele" z udziałem Komisarz Elżbiety Bieńkowskiej. Spotkanie zorganizowane zostało wraz z Przedstawicielstwem Komisji Europejskiej w Polsce. Wszystkim przybyłym dziękujemy za udział w spotkaniu. Tym, którzy nie mieli możliwości uczestniczyć w DO przypominamy, że relacja ze spotkania dostępna jest na kanale <a href="https://www.youtube.com/watch?v=bBLCOxpHZ0c">YouTube Przedstawicielstwa KE w Polsce </a>
								
								</p>
							
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="#"><img src="img/logotypy/iss.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="https://ec.europa.eu/commission/index_pl"><img src="img/logotypy/komisja_europejska.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href=" "><img src="img/logotypy/chodzi_o_europe.jpg" /></a></li>
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
				<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item">
						<div class="row aktualnosc-bg">
							<p class="date">29-08-2017</p>
							<div class="col-md-4">
								<a href="news/29-08-2017/zaproszenie.pdf"><img src="news/29-08-2017/zaproszenie.jpg" alt="Prezydent" class="img-responsive"></a>
							</div>
							<div class="col-md-8 text" style="text-align:center">
								<h2>
								BEZPIECZNIEJSZA POLSKA, BEZPIECZNIEJSZA EUROPA,<br>BEZPIECZNIEJSI OBYWATELE
								</h2>
								<p>
								PRZEDSTAWICIELSTWO KOMISJI EUROPEJSKIEJ W POLSCE ORAZ FUNDACJA INSTYTUT STUDIÓW STRATEGICZNYCH ZAPRASZAJĄ DO UDZIAŁU W
DIALOGU OBYWATELSKIM: <br><b>„BEZPIECZNIEJSZA POLSKA, BEZPIECZNIEJSZA EUROPA, BEZPIECZNIEJSI OBYWATELE”</b><br><br>
GOŚĆ SPECJALNY<br>
									<b>ELŻBIETA BIEŃKOWSKA</b> <br>
KOMISARZ DS. RYNKU WEWNĘTRZNEGO, PRZEMYSŁU, PRZEDSIĘBIORCZOŚCI ORAZ<br> MAŁYCH I ŚREDNICH PRZEDSIĘBIORSTW <br>
<br><br>
godz. 11:00-12:30; Hotel Holiday Inn Garden; Wielopole 4, Kraków<br>
Potwierdzenia przybycia do poniedziałku 4 wrzenia:<br> tel. 12 421 62 50; mail: biuro@iss.krakow.pl<br>
<br><b>Liczba miejsc ograniczona.</b> <br> <br>
<a href="news/29-08-2017/Dialog_Obywatelski_opis.pdf">pobierz opis wydarzenia</a>							
								</p>
							
								
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners">

									 <li  style="padding-right:60px;"><a href="#"><img src="img/logotypy/iss.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href="https://ec.europa.eu/commission/index_pl"><img src="img/logotypy/komisja_europejska.jpg" /></a></li>
									 <li  style="padding-right:60px;"><a href=" "><img src="img/logotypy/chodzi_o_europe.jpg" /></a></li>
									


								 </ul> 
							 </div>
						 </div>
						

					   </div>
				
<!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
					<div class="item ">
						<div class="row aktualnosc-bg">
							<p class="date">25-08-2017</p>
							<div class="col-md-4">
								<img src="news/28-05-2017/brzezinski.jpg" alt="Prezydent" class="img-responsive">
							</div>
							<div class="col-md-8 text">
								<h2>
									żegnamy Zbigniewa Brzezińskiego
								</h2>
								<p>
								Rada Fundatorów, Zarząd i Zespół Fundacji Instytut Studiów Strategicznych z wielkim smutkiem żegnają Zbigniewa Brzezińskiego, pomysłodawcę naszego Instytutu i członka naszej Międzynarodowej Rady Honorowej. 
								</p>
							</div>
							
						</div>
					
								 <a class="left carousel-control" href="#aktualnosci-slide" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								 </a>
								 <a class="right carousel-control" href="#aktualnosci-slide" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								 </a>
						
						<div class="row">
							<div class="col-md-4"></div>	
							<div class="col-md-8">
							 
								 <ul class="logo-partners" style="visibility: hidden">

									 <li><a href=" "><img src="img/logotypy/comarch.jpg" /></a></li>
									 <li><a href=" "><img src="img/logotypy/balice.jpg" /></a></li>
									 <li><a href=" "><img src="img/logotypy/gazeta_wyborcza.jpg" /></a></li>
									 <li><a href=" "><img src="img/logotypy/interia.jpg" /></a></li>
									 <li><a href=" "><img src="img/logotypy/eunic.jpg" /></a></li>


								 </ul>
							 </div>
						</div>
				</div>
							   
		 <!-- /////////////////////////////////////POJEDYNCZA AKTUALNOŚĆ////////  -->
		