<div class="container-fluid" id="contact" >
	
		<div class="row">
			<div class="col-md-4">
			<div class="col-md-4">
				</div>
			<div class="col-md-8">
				<address>
					KONTAKT:<br><br>
					Fundacja Instytut Studiów Strategicznych<br>
					ul. Mikołajska 4,<br>
					31-027 Kraków,<br>
					tel./fax: +48 12 421 62 50<br>
					NIP: 676-01-02-235<br>
					Sekretariat czynny:<br>
					od poniedziałku do piątku <br>od godz. 8.30 do godz. 16.30

				</address>
				</div>
			</div>
		
			<div id="map" class="col-md-8">
				
			</div>
		</div>
		<div class="row">
			<footer class="col-xs-12 text-center" >
				<p>copyright© 2017 INSTYTUT STUDIÓW STRATEGICZNYCH</p>
			</footer>
		</div>
	
</div>	
	
	
<!--MAPA--> 
         
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="//maps.google.com/maps/api/js?sensor=false&amp;language=pl"></script>
        <script src="../js/gmap3.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyCYI6CJajUeR8w51NGQ5TLX6EgBSy7-PUc"></script>
        
<script type="text/javascript">
	$('#map').gmap3({
    map: {
        options: {
            center: [50.061402, 19.941382],
            zoom: 15,
            panControl: false,
            zoomControl: false
        }
    },
    marker: {
        values:[{
            latLng: [50.061402, 19.941382],
            data: 'ISS'
        }],
        events:{
            click: function(marker, event, context) {
                var map = $(this).gmap3('get'),
                    infowindow = $(this).gmap3({get:{name:'infowindow'}});
                if (infowindow) {
                    infowindow.open(map, marker);
                    infowindow.setContent(context.data);
                } else {
                    $(this).gmap3({
                        infowindow: {
                            anchor: marker,
                            options: {content: context.data}
                        }
                    });
                }
            }
        }
    }
});
</script>
    
  <!--MAPA-->   
    <script src="../js/wlasny.js"></script> 
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    
    
  </body>
</html>