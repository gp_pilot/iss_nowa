<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="../../css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="../../css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="../../index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="../../img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">24-03-2018</h1>
            	<h2>Narodowy Dzień Pamięci Polaków ratujących Żydów pod okupacją niemiecką. </h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Dzisiaj obchodzimy Narodowy Dzień Pamięci Polaków ratujących Żydów pod okupacją niemiecką. 

Z tej okazji prezentujemy sylwetki czterech osób nagrodzonych Medalem „Sprawiedliwy wśród Narodów Świata”.

               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
					<b>Bartoszewski, Władysław</b><br>
Od września 1942 roku, jeszcze przed utworzeniem „Żegoty”, Władysław Bartoszewski był członkiem Tymczasowego Komitetu Pomocy Żydom. Gdy w grudniu 1942 roku utworzono „Żegotę”, Bartoszewski (pseudonim „Ludwik”) całym sercem zaangażował się w pracę
dla organizacji, ale też pomagał wielu żydowskim uciekinierom na własną rękę. W „Żegocie” Bartoszewski reprezentował Front Odrodzenia Polski, tajną organizację katolicką i pracował jako zastępca szefa Referatu Żydowskiego w Departamencie Spraw Wewnętrznych
Delegatury Rządu. Bartoszewski ocalił życie wielu Żydom, którzy po ucieczce z getta warszawskiego ukrywali się po „aryjskiej” stronie miasta lub na prowincji.<br>
Zaangażowany w podziemną działalność, utrzymywał ścisłe kontakty z żydowskimi przedstawicielami „Żegoty”, w tym z Leonem Feinerem (Fajnerem), reprezentującym
Bund, i Adolfem Bermanem, reprezentantem Żydowskiego Komitetu Narodowego. Do obowiązków Bartoszewskiego należało także wysyłanie do Anglii i Stanów Zjednoczonych informacji na temat sytuacji Żydów w Polsce pod okupacją niemiecką. Później pisarka
Rachela Auerbach i dr Adolf Berman zaświadczyli, że podczas okupacji Bartoszewski, po wojnie szanowany dziennikarz i publicysta, podejmował wszelkie wysiłki, aby polska opinia publiczna dowiedziała się o heroizmie polskich Żydów. Opublikował wiele artykułów
i esejów cenionych za obiektywność i życzliwość wobec narodu żydowskiego, a także przyczynił się do budowy wzajemnego zbliżenia między Polakami a Żydami.
14 grudnia 1965 r. Instytut YadVashem nadał Władysławowi Bartoszewskiemu tytuł Sprawiedliwego wśród Narodów Świata.<br><br>

<b>Małysiak, Albin<br>
	Wilemska, Bronisława</b><br>
Podczas okupacji ksiądz Albin Małysiak i siostra Bronisława Wilemska pomogli pięciorgu Żydom. W tamtym czasie siostra Bronisława kierowała domem dla osób starszych i upośledzonych w Krakowie, gdzie ksiądz Albin był kapelanem. W 1943 roku pięcioro
Żydów przybyło do domu i pozostało w nim jako podopieczni. Byli to: Katarzyna Styczeń (lat 45), Helena Kachel (50), Zbigniew Koszanowski (40 lat) Henryk Juański (30 lat) i jeszcze jeden mężczyzna, w wieku około 30-35 lat. Zostali zaopatrzeni w fałszywe dokumenty, jedzenie i ubrania. „Pomogliśmy im z powodów czysto humanitarnych. Jezus Chrystus nauczał, aby
kochać każdego” – pisał ksiądz Albin w swoim oświadczeniu do YadVashem. Wiosną 1944 roku podopiecznych  domu, łącznie z siostrami, pielęgniarkami i personelem świeckim, Niemcy przepędzili do Szczawnicy Wyżnej (powiat Nowy Targ, dystrykt krakowski). Pięcioro
Żydów, jako podopieczni zakładu, także trafiło do Szczawnicy. „Niemal wszyscy mieszkający
w domu wiedzieli, że siostra Wilemska i ja ukrywamy Żydów” – pisał ksiądz Albin. Wielu mieszkańców Szczawnicy także o tym wiedziało, nikt jednak nie poinformował władz, chociaż w okolicy znajdował się posterunek policji niemieckiej. Helena Kachel zmarła jesienią 1944 roku, a niedługo potem także Katarzyna Styczeń. Mężczyźni przetrwali do wyzwolenia w styczniu 1945 roku. Córka Katarzyny, Maria Rolicka, po otrzymaniu informacji o śmierci matki, przyjechała do Szczawnicy. „Rozmawiałam z siostrami i wielebnym ojcem, którzy pomogli mojej matce i czwórce pozostałych Żydów” – pisała. Ksiądz Albin powiedział jej,
że odbył z jej matką „wiele długich rozmów. Spacerowaliśmy w Górnym Parku w Szczawnicy i dyskutowaliśmy na temat różnych problemów Żydów, Polaków i ogólnie ludzkości” – wspominała Maria Rolicka. 
21 listopada 1993 r. Instytut YadVashem nadał księdzu biskupowi Albinowi Małysiakowi tytuł Sprawiedliwego wśród Narodów Świata. 25 kwietnia 1995 r. Instytut YadVashem nadał siostrze
Bronisławie Wilemskiej tytuł Sprawiedliwej wśród Narodów Świata.<br><br>

					<b>Pankiewicz, Tadeusz</b><br>
Polski aptekarz, Tadeusz Pankiewicz, przekupił niemieckie władze w Krakowie, żeby móc nadal prowadzić aptekę na terenie getta. Jako farmaceuta, Pankiewicz  oddał się do dyspozycji zamieszkujących getto Żydów. Nie tylko zaopatrywał ich w lekarstwa, ale zamienił
też swoją aptekę w miejsce spotkań inteligencji getta. Żydzi spotykali się tam, żeby zdobyć aktualne wiadomości spoza getta i utrzymywać kontakty z ludnością z „aryjskiej” strony miasta. Nie bacząc na zagrożenie życia, Pankiewicz wykorzystywał aptekę, żeby pomagać Żydom i ich ratować. Podczas jednej z „akcji” przeprowadzonych przez Niemców w getcie Pankiewicz
ukrył w aptece doktora Abrahama Mirowskiego wraz z Ireną Cynowicz (z domu Halpern), osadzoną w getcie w 1942 roku. Cynowicz udało się uciec z grupy Żydów zabranych do getta podczas „akcji”, a Pankiewicz schował ją pod ladą i zasłonił własnym ciałem,  ratując ją przed zabraniem do transportu. Irena znała wcześniej Pankiewicza, ponieważ odwiedzała jego aptekę,
żeby kupić lekarstwa dla chorej matki. Pankiewicz odmówił przyjęcia pieniędzy, a później Irena dowiedziała się, że nie była jedyną osobą w getcie, która otrzymywała lekarstwa za darmo. Działania Pankiewicza wynikały z pobudek humanitarnych i patriotycznych, a wielu z tych, którym pomógł, zawdzięcza mu życie. Mirowski i Cynowicz, którzy po wojnie wyemigrowali
do Izraela, zaświadczyli po latach o wielu działaniach Pankiewicza, mających na celu ocalenie krakowskich Żydów. Po latach Pankiewicz napisał wspomnienia pt.: Apteka w getcie krakowskim, opublikowane także przez Instytut YadVashem w języku hebrajskim, gdzie przedstawił obraz codziennego życia w getcie: cierpienie Żydów, ich wytrwałość i życie na co dzień, a także brutalność Niemców.
10 lutego 1983 r. Instytut YadVashem nadał Tadeuszowi Pankiewiczowi tytuł Sprawiedliwego wśród Narodów Świata.<br><br>

Wszystkich zainteresowanych zapraszamy do lektury naszych publikacji: „Sprawiedliwi i ich świat. Markowa w fotografii Józefa Ulmy” oraz „Księga Sprawiedliwych wśród Narodów Świata”.


				</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("../../inc/footer.php");
?>
