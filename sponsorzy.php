<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Aktualności - Prosta Edukacja</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Sponsorzy ISS</h1>
            	<h2>Sponsoring wydarzeń organizowanych przez Fundację Instytut Studiów Strategicznych </h2>
            
            </div>
        </div>
    </section>
     <section class="container"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Zapraszamy Państwa do współpracy z Fundacją Instytut Studiów Strategicznych w roli sponsora.                 	<!--	<span>2016-07-31</span> -->
              	 </header>
            	

    <p class=" my-content">
		Sponsoring wydarzeń organizowanych przez Fundację Instytut Studiów Strategicznych to ważny element każdego realizowanego przez Fundację przedsięwzięcia. Tematyka oraz grono znamienitych gości, zapraszanych do udziału w poszczególnych wydarzeniach i konferencjach, gwarantują wspierającym je instytucjom prestiż oraz rozpoznawalność na polu nie tylko krajowym, ale i międzynarodowym. Dodatkowo uczestnictwo w nich, to doskonała okazja do nawiązania licznych kontaktów. Dzięki wsparciu firm czy instytucji mamy środki na realizowanie wyznaczonych nam zadań na najwyższym poziomie.<br><br>
		Zapraszamy Państwa do współpracy z Fundacją Instytut Studiów Strategicznych w roli sponsora.<br>
    Ze swojej strony gwarantujemy: <br><br>
    umieszczenie logo Sponsora na ulotkach i plakatach informacyjnych, <br><br>
    umieszczenie logo Sponsora we wszelkich informacjach dotyczących danego wydarzenia, które znajdą się na naszej stronie internetowej oraz w zakładce Sponsorzy, a także kontach na portalach społecznościowych, <br><br>
    umieszczenie logo Sponsora w prezentacjach wyświetlanych podczas trwania poszczególnych wydarzeń a także podziękowania za współprace (wymienienie sponsora z nazwy), <br><br>
    logo lub nazwa na reklamach w mediach (radio, TV, prasa codzienna i opiniotwórcza, Internet) – w zależności od wysokości środków, <br><br>
    możliwość ustawienia reklamy (baner, roll-up) podczas konferencji, <br><br>
    możliwość rozdysponowania własnych materiałów promocyjnych w trakcie trwania poszczególnych wydarzeń, <br><br>
    otrzymanie puli zaproszeń na poszczególne wydarzenia

	</p>

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
