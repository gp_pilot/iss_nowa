<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Polska Polityka Wschodnia
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
          Od początku procesu transformacji w Europie Środkowej i Wschodniej Polska dążyła do budowania dobrosąsiedzkich stosunków ze swymi wschodnimi sąsiadami i była rzecznikiem prowadzenia przez UE aktywnej polityki wobec tych krajów oraz wspomagania przez nią procesów reform w Europie Wschodniej. Fundacja Instytut Studiów Strategicznych aktywnie włącza się w te działania.
Głównym założeniem programu jest określenie pożądanego kształtu stosunków Polski z jej wschodnimi sąsiadami, szczególnie w kontekście członkostwa Polski w Unii Europejskiej. Dokładnej analizie poddawana jest polityka zagraniczna wschodnich sąsiadów Polski, zagadnienia związane z bezpieczeństwem oraz zapobieganie konfliktom w regionie. Przedmiotem zainteresowania są także przemiany ekonomiczne zachodzące u naszych wschodnich sąsiadów, między innymi wprowadzenie mechanizmów gospodarki rynkowej, reformy systemów podatkowych, bankowych i prywatyzacja. Podejmowane w ramach programu działania mają na celu nie tylko analizę sytuacji czy zdefiniowanie problemów, ale także wsparcie procesów demokratycznych oraz grup, które się szczególnie przyczyniają do rozwoju społeczeństwa obywatelskiego.
 <br> 

<ul>
	<b>Ważniejsze wydarzenia programu "Polska Polityka Wschodnia":  </b><br>
	<li>konferencja międzynarodowa Brainstorming Session "Ukraine as Part of Wider Europe" – czerwiec 2004</li>
<li>programy stażowe dla przedstawicieli samorządów terytorialnych, organizacji pozarządowych i lokalnych liderów z Ukrainy, Białorusi oraz Mołdawii – 2005, 2006, 2007, 2008</li>
<li>konferencja międzynarodowa "Ukraina i Białoruś 2007" – październik 2007</li>
<li>projekt "Społeczeństwo i Armia – wzajemne zaufanie podstawą dla wzmocnienia społeczeństwa obywatelskiego" (raport "NGO i administracja. Stan obecny współpracy", warsztaty dla liderów NGOs, konferencja ekspercka) – 2008</li>
<li>Spotkanie z Lasha Zhvania, Przewodniczącym Komisji Spraw Zagranicznych Parlamentu Gruzji – wrzesień 2008</li>
<li>Nato-Ukraina. Konferencja dotycząca planowania układu partnerstwa: "Udział Społeczeństwa Obywatelskiego w Reformie Sektora Bezpieczeństwa: Dotychczasowe Doświadczenia i Rekomendacje na Przyszłość" – październik 2009</li>


			
			
				</ul>
				</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
