<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Sprawozdania ISS</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet">     
    <link rel="stylesheet" href="css/font-awesome.min.css">
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/animate.min.css" rel="stylesheet" media="screen">

      
      
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
	<header class="container-fluid">
    	<div class="row">
        	<div class="col-xs-offset-4 col-xs-8 white-header">
            	<a href="index.php"><button type="button" class="btn btn-success"><i class="fa fa-home fa-2x "></i></button></a>
            </div>
         </div>
    </header>
    <section class="container-fluid">
    	<div class="row">
        	<div class=" col-xs-12 col-sm-12  col-md-4 left-cont text-center img-responcive">
            	<img src="img/logo.png" alt="logo">
                <p>Fundacja<br>Instytut Studiów Strategicznych</p>
            </div>
            <div class=" col-xs-12 col-sm-12 col-md-8  right-cont-header">
            	<h1 class="name">Programy ISS</h1>
            	<h2> "Opisy programów edukacyjnych i badawczych"</h2>
            
            </div>
        </div>
    </section>
     <section class="container program"> 
     		<div class=" col-xs-12  right-cont">
 	 <!-- Aktualność-->
            	<header class="my-news-tittle ">Sprawiedliwi i ich świat - spotkania z historią dla młodzieży ponadgimnazjalnej
               	<!--	<span>2016-07-31</span> -->
              	</header>
            	<p>
        Instytut Studiów Strategicznych od 2008 roku realizuje projekt edukacyjny „Sprawiedliwi i ich świat” - spotkania z historią dla młodzieży ponadgimnazjalnej. Głównym założeniem projektu jest przybliżenie młodzieży polskich obywateli uhonorowanych tytułem Sprawiedliwy wśród Narodów Świata. Życiorysy Sprawiedliwych są dla młodych ludzi wyjątkowym źródłem istotnych wartości i inspirujących postaw. Celem projektu jest nie tylko zaznajomienie uczniów z wyżej wymienioną tematyką, ale również poprzez osadzenie jej w kontekście lokalnym, sprawienie, aby uczniowie mogli odczuć i zrozumieć, że wielkie wydarzenia historyczne dotyczą także małych ojczyzn, historii ich regionu a także często nawet losów rodzinnych samych zainteresowanych. Takie podejście pozwala na efektywniejsze przekazywanie wiedzy.<br>

Każda z edycji projektu składa się z dwóch części: 40 trzygodzinnych spotkań z młodzieżą w ich szkole. Animatorzy prowadząc zajęcia z młodzieżą przekazują wiedzę na temat wojny, Holokaustu, bestialstwa Niemców oraz postaw Polaków w stosunku do Żydów na przykładzie wydarzeń, które miały miejsce w miejscowości, w której prowadzone były zajęcia, bądź gdy konkretne dane były niedostępne, przedstawiali historię miejscowości sąsiedniej. W trakcie lekcji prezentowane są między innymi biogramy Sprawiedliwych, pochodzące z Księgi Sprawiedliwych wśród Narodów Świata, której pierwszą polską edycję wydał Instytut Studiów Strategicznych w 2009 r. <br>

W ramach programu Sprawiedliwi i ich świat Instytut Studiów Strategicznych zorganizował również serię cyklicznych konferencji naukowych poświęconych pamięci Polskich Sprawiedliwych wśród Narodów Świata. <br>

28 czerwca 2010 r. w Krakowie odbyła się międzynarodowa konferencja pt. „Ten jest z ojczyzny mojej, jest człowiekiem - Polscy Sprawiedliwi wśród Narodów Świata”, zorganizowana przy współpracy z Narodowym Centrum Kultury. Wykład wprowadzający do konferencji wygłosił prof. Władysław Bartoszewski, a wśród zaproszonych gości znaleźli się między innymi: Sprawiedliwi skupieni w Polskim Towarzystwie Sprawiedliwych wśród Narodów Świata z Krakowa i Warszawy, Ambasador Izraela w Polsce Zvi Rav – Ner; prof. Feliks Tych z Żydowskiego Instytutu Historycznego, historycy z Instytutu Pamięci Narodowej oraz Państwowego Muzeum na Majdanku, Ambasador dr Maciej Kozłowski z Ministerstwa Spraw Zagranicznych RP oraz przedstawiciele jednostek samorządu terytorialnego, organizacji pozarządowych, instytucji kultury, mediów, ośrodków badawczych 
i akademickich.


					</p>
 					

          
           <!-- Aktualność-->
            <!-- Aktualność-->
            	
</div>
	</section>

<?php
		 include("inc/footer.php");
?>
